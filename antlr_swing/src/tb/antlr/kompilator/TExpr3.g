tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer scope = 0;
}

/*Zrobione zadania:
  -Zadanie podstawowe
  -Zadanie dodatkowe 4 - instrukcje warunkowe
*/

prog    : (e+=block| e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
    ;
    
block : ^(LCB {enterScope(); scope++;} (e+=block | e+=expr | d+=decl)* {leaveScope(); scope--;}) -> block(ex={$e}, dec={$d})
    ;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> podstaw(name={$i1.text}, value={$e2.st})
        | INT                 -> int(i={$INT.text})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;} -> if(c={$e1.st},t={$e2.st},e={$e3.st}, nr={numer.toString()})
        | ID                    -> id(nazwa={$ID.text})
    ;
    