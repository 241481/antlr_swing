tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import java.util.HashMap;
import tb.antlr.symbolTable.LocalSymbols;
}

@members {
LocalSymbols symbols = new LocalSymbols();
}

prog    : ( ^(VAR i1=ID) {symbols.newSymbol($i1.text);}
          | ^(PODST i1=ID e2=expr) {symbols.setSymbol($i1.text, $e2.out);}
          | ^(PRINT e1=expr) {drukuj ($e1.text + "=" +$e1.out.toString());}
          | LCB {symbols.enterScope();}
          | RCB {symbols.leaveScope();}
          )*
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD e1=expr e2=expr)   {$out = $e1.out \% $e2.out;}
        | ^(ABS e1=expr)       {$out = Math.abs($e1.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = symbols.getSymbol($ID.text);}
        ;
