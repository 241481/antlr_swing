grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

/*Zrobione zadania:
  -Zadanie podstawowe
  -Zadania dodatkowe 4 - instrukcje warunkowe
*/

prog
    : (stat | block)+ EOF!;
    
block
    : LCB^ (stat | block)* RCB!
    ;
    
stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | if_stat NL -> if_stat
    | NL ->
    ;
    
if_stat
    : IF^ expr THEN! (block|expr) (ELSE! (block|expr))?
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    | ABS expr ABS -> ^(ABS expr)
    ;

PRINT: 'print' ;

VAR :'var';

IF: 'if' ;

THEN: 'then' ;

ELSE: 'else' ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
MOD
  : '%'
  ;
  
ABS
  : '|'
  ;
  
LCB
  : '{'
  ;
  
RCB
  : '}'
  ;
